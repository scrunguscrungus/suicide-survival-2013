//=============================================================================//
//
// Purpose: Self destruct weapon for shrubberies
//
//=============================================================================//
#include "cbase.h"

#ifdef CLIENT_DLL
	#include "c_hl2mp_player.h"
#else
	#include "hl2mp_player.h"
	#include "explode.h"
#endif

#include "weapon_hl2mpbasehlmpcombatweapon.h"	


#include "soundent.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


extern ConVar sk_fraggrenade_radius;
extern ConVar sk_plr_dmg_fraggrenade;

#ifdef CLIENT_DLL
#define CWeaponSuicider C_WeaponSuicider
#endif

class CWeaponSuicider : public CBaseHL2MPCombatWeapon
{
	DECLARE_CLASS( CWeaponSuicider, CBaseHL2MPCombatWeapon );

	public:
	DECLARE_NETWORKCLASS( );
	DECLARE_PREDICTABLE( );

	CWeaponSuicider( );

	virtual void PrimaryAttack( );
	virtual void SecondaryAttack( );

	virtual void Precache( );

	private:
	float m_flNextTaunt;
};


IMPLEMENT_NETWORKCLASS_ALIASED( WeaponSuicider, DT_WeaponSuicider )

BEGIN_NETWORK_TABLE( CWeaponSuicider, DT_WeaponSuicider )

END_NETWORK_TABLE( );

#ifdef CLIENT_DLL
BEGIN_PREDICTION_DATA( CWeaponSuicider )
END_PREDICTION_DATA( )
#endif

LINK_ENTITY_TO_CLASS( weapon_suicider, CWeaponSuicider );
PRECACHE_WEAPON_REGISTER( weapon_suicider );

CWeaponSuicider::CWeaponSuicider( )
{

}

void CWeaponSuicider::PrimaryAttack( )
{
	float m_DmgRadius = 256.0f;
	float m_flDamage = 9999.0f;

#if !defined( CLIENT_DLL )
	Vector vecAbsOrigin = GetOwner()->GetAbsOrigin();


	CPASFilter filter( vecAbsOrigin );
	te->Explosion( filter, -1.0, // don't apply cl_interp delay
				   &vecAbsOrigin,
				   g_sModelIndexFireball,
				   m_DmgRadius * .03,
				   25,
				   TE_EXPLFLAG_NONE,
				   m_DmgRadius,
				   m_flDamage );

	CSoundEnt::InsertSound( SOUND_COMBAT, GetAbsOrigin( ), 1024, 3.0 );


	// Use the thrower's position as the reported position
	Vector vecReported = GetOwner( )->GetAbsOrigin( );


	CTakeDamageInfo info( GetBaseEntity(), GetOwner(), vec3_origin, GetAbsOrigin( ), m_flDamage, DMG_BLAST, 0, &vecReported );

	RadiusDamage( info, GetAbsOrigin( ), m_DmgRadius, CLASS_NONE, NULL );


	EmitSound( "BaseGrenade.Explode" );

#endif
}

void CWeaponSuicider::SecondaryAttack( )
{
	if ( m_flNextTaunt <= gpGlobals->curtime )
	{
		CBasePlayer *pOwner = GetPlayerOwner( );
		if ( pOwner )
		{
			pOwner->EmitSound( "Taunt.Suicider" );
			m_flNextTaunt = gpGlobals->curtime + 1.0f;
		}
	}
}

void CWeaponSuicider::Precache( )
{
	BaseClass::Precache( );

	PrecacheScriptSound( "Taunt.Suicider" );
}
